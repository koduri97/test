const mongoose = require('mongoose');

const configSchema = new mongoose.Schema({
    newsSource : String
});

module.exports = mongoose.model("Config", configSchema);
const express        = require("express");
const bodyParser     = require("body-parser");
const holidays       = require('public-holidays');
const mongoose       = require('mongoose');
const util           = require('util');
const app            = express();
const DynamicFetch   = require('./config/config');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended : false }));

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, DELETE, OPTIONS"
  );
  next();
});


var retEvent, retTime;
app.use("/api/holidays", (req, res, next) => {

  var filter = {country: 'in', lang: 'en'};
      
      holidays(filter, (error, result) => {
      if(!error){
          hArray = [];
          var date = new Date(), year = date.getFullYear(), monthId = date.getMonth(), month = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
        
          for(var k=0;k<6;k++){
            for(var i=0; i<result.length; i++)
            {   
                if((""+result[i].start).indexOf(""+month[monthId])>=0 && (""+result[i].start).indexOf(""+year)>=0){
                  var str1 = result[i].summary, str2="";
                    if(k==0 && date.getDate()>parseInt((""+result[i].start).substr(8,2))){
                      str1="";
                      continue;
                    }
                    else if(k==0){
                      if((parseInt((""+result[i].start).substr(8,2))-date.getDate())===1){
                        str2+="In a day";
                      }
                      else {str2+="In "+(parseInt((""+result[i].start).substr(8,2))-date.getDate())+" days";}
                    }
                    else{
                      if(k==1){
                        str2+="In a month";
                      }
                      else{
                        str2+="In "+k+" months";
                      }
                    }
                  hArray.push({
                    holidayEvent : str1,
                    holidayTime : str2
                  });
                  if(hArray.length>=7)break;
                }
            }
            if(hArray.length>=7)break;
            monthId++;
            if(monthId>11)year++;
            monthId%=12;
          }
          res.status(200).json({
            message: "Holidays fetched succesfully!",
            holidays: hArray
          });
        }
      });
});


var dynamicFetch = new DynamicFetch();
dynamicFetch.init();

app.use("/api/configs", (req, res, next) => {
  
  dynamicFetch.fetch(res);

});
module.exports = app;

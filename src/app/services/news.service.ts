import { Injectable,NgModule } from '@angular/core';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  //   this.http.get('http://localhost:3000/api/configs')
  //     .subscribe((configData)=>{
  //         this.cObject = configData;
  //         console.log('Kyu');
  //     });
  paramSource : string;
  constructor(private http : Http, private configService : ConfigService) { 
    console.log("NewsService Initialized");
    
  }
  ngOnInit(){
  } 
  // fetchConfigs = (callback) => {
  //   this.http.get('http://localhost:3000/api/configs')
  //     .subscribe((configData)=>{
  //         this.cObject = configData;
  //         console.log('Kyu');
  //     });
  //   return callback();
  // }
  // getNewsWithConfigs = () => {
  //       console.log('Hiiiiiii');
  //       this.myPromise.then(function(){
  //         if(this.cObject.configSettings.newsSource === 'default'){
  //           return this.http.get('https://newsapi.org/v2/top-headlines?country=in&apiKey=b3712c5d9992406aa5c0c5d396000915')
  //           .pipe(map(res => res.json()));
  //         }
  //         else if(this.cObject.configSettings.newsSource === 'toi'){
  //           return this.http.get('https://newsapi.org/v2/top-headlines?sources=the-times-of-india&apiKey=b3712c5d9992406aa5c0c5d396000915')
  //           .pipe(map(res => res.json()));
  //         }
          
  //       });
        
  // }
  getNews = (configs) => {
    console.log('Here\n\n\n'+configs.configSettings.newsSource);
          
          if(configs.configSettings.newsSource === 'default'){
            return this.http.get('https://newsapi.org/v2/top-headlines?country=in&apiKey=b3712c5d9992406aa5c0c5d396000915')
            .pipe(map(res => res.json()));
          }
          else if(configs.configSettings.newsSource === 'toi'){
            //this.paramSource = 'the-times-of-india';
            return this.http.get('https://newsapi.org/v2/top-headlines?sources=the-times-of-india&apiKey=b3712c5d9992406aa5c0c5d396000915')
            .pipe(map(res => res.json()));
          }
    // return this.fetchConfigs(this.getNewsWithConfigs);
 }
}

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, pipe } from 'rxjs';
import { map } from 'rxjs/operators';
import  'rxjs/add/operator/toPromise';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  cObject;
  constructor(private http : Http) {
    console.log('ConfigService Initialized')
  }
  getConfig(){
   return this.http.get('http://localhost:3000/api/configs')
    .pipe(map((response)=>response.json()
    ));
  }
}

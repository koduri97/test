import { Injectable,NgModule } from '@angular/core';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class WeatherService {
  lat : number;
  lng : number;
  constructor(private http : Http) { 
    console.log("WeatherService Initialized");
  }
  getWeather(data : Array<number>){
    this.lat = data[0];
    this.lng = data[1];
    return this.http.get('https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(SELECT%20woeid%20FROM%20geo.places%20WHERE%20text%3D%22('+this.lat+'%2C'+this.lng+')%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys')
    .pipe(map(res => res.json()));
  }
}

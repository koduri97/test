import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HelloWorldComponent } from './components/hello-world/hello-world.component';
import { ClockComponent } from './components/clock/clock.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { WeatherComponent } from './components/weather/weather.component';
import { NewsComponent } from './components/news/news.component';
import { HolidaysComponent } from './components/holidays/holidays.component';
import { WeatherService } from './services/weather.service';
import { NewsService } from './services/news.service';

@NgModule({
  declarations: [
    AppComponent,
    HelloWorldComponent,
    ClockComponent,
    CalendarComponent,
    WeatherComponent,
    NewsComponent,
    HolidaysComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule
  ],
  providers: [WeatherService, NewsService],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component } from '@angular/core';
import * as publicHolidays from 'public-holidays';
declare var pubHolidays: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
}

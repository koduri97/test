import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hello-world',
  templateUrl: './hello-world.component.html',
  styleUrls: ['./hello-world.component.css']
})
export class HelloWorldComponent implements OnInit {
  
  text : string;
  myDate : Date;
  hours : number;
  curr_rnd : number;
  prev_rnd : number;
  pprev_rnd : number;
  anytime : string[];
  morning : string[];
  evening : string[];
  afternoon : string[];
  night : string[];
  selectArray : number;
  startHelloWorld : boolean;
  
  constructor() { 
    this.prev_rnd = -1;
    this.pprev_rnd = -1;
    this.startHelloWorld = false;
    this.anytime = ['Hey there sexy!', 'Hello, sunshine!', 'You\'re the smartest cookie in the jar :)', 'Hello, genius!', 'You\'re peachy perfect!' ];
    this.text = this.anytime[(this.prev_rnd=Math.floor(Math.random()*(this.anytime.length)))];
    this.startHelloWorld = true;
    this.morning = ['How was your sleep?', 'Enjoy your day!', 'Good Morning!', 'You shine brighter than Sirius!'];
    this.afternoon = ['Looking good today!', 'You look great!', 'Hello, beauty!', 'Good Afternoon!'];
    this.evening = ['Wow, you look hot!', 'You look nice!', 'Hi, sexy!', 'Good evening!', 'You\'re more attractive than a magnet!'];
    this.night = ['Good night!', 'Have a sound sleep!', 'You\'re pure magic!'];
    this.getCompliment();
  }

  ngOnInit() {
  }
  getCompliment() : void {
    setInterval(()=>{
      this.myDate = new Date();
      this.hours = this.myDate.getHours();
      this.startHelloWorld = false;
      if(this.hours>=0 && this.hours<12){
        this.getRandom(this.morning);
      }
      else if(this.hours>=12 && this.hours<17){
        this.getRandom(this.afternoon);
      }
      else if(this.hours>=17 && this.hours<20){
        this.getRandom(this.evening);
      }
      else{
        this.getRandom(this.night);
      }
    },30000);
  }
  getRandom(array : string[]) : void {
    this.curr_rnd = Math.floor(Math.random()*(array.length));
    //console.log(this.curr_rnd);
    while(this.prev_rnd == this.curr_rnd || this.pprev_rnd == this.curr_rnd){
      this.curr_rnd = Math.floor(Math.random()*(array.length));
    }
    this.pprev_rnd = this.prev_rnd;
    this.prev_rnd = this.curr_rnd;
    this.selectArray = Math.floor(Math.random()*(2));
    //console.log(this.pprev_rnd+" "+this.prev_rnd+" "+this.curr_rnd);
    if(this.selectArray==1)
      this.text = array[this.curr_rnd];
    else this.text = this.anytime[this.curr_rnd]; 
    this.startHelloWorld = true;
    //console.log(this.text);
  }
  

}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { WeatherService } from '../../services/weather.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css'],
  providers: [WeatherService]
})
export class WeatherComponent implements OnInit, OnDestroy {
  reply;
  forecasts : forecast[];
  hours : number;
  lat : number;
  lng : number;
  tdy : Array<string>;
  data : Array<number>;
  date : Date;
  private weatherSub : Subscription;
  constructor(private weatherService : WeatherService) { 
    this.data = [];
    this.tdy = [];
    this.forecasts = [];
    if (window.navigator && window.navigator.geolocation) {
        
        window.navigator.geolocation.getCurrentPosition(
            position => {
                this.date = new Date();
                this.hours = this.date.getHours();
                this.lat = position.coords.latitude;
                this.lng = position.coords.longitude;
                //console.log(this.lat+" "+this.lng);
                    this.data.push(this.lat);
                    this.data.push(this.lng);
                    //console.log(this.lat+" "+this.lng);
                    this.weatherSub = this.weatherService.getWeather(this.data).subscribe(weather => {
                        //console.log("inside");
                        this.reply = weather.query.results.channel;
                        //console.log(this.reply);
                        this.tdy.push(((this.reply.item.condition.temp-32)*5/9).toFixed(1));
                        this.tdy.push(this.reply.item.condition.text);
                         
                         for(var i=1; i<=7; i++){
                            this.forecasts.push(
                                {day : this.reply.item.forecast[i].day,
                                 high : ((this.reply.item.forecast[i].high-32)*5/9).toFixed(1),
                                 low : ((this.reply.item.forecast[i].low-32)*5/9).toFixed(1),
                                 text : this.reply.item.forecast[i].text
                                });
                         }
                    });
            },
            error => {
                switch (error.code) {
                    case 1:
                        console.log('Permission Denied');
                        break;
                    case 2:
                        console.log('Position Unavailable');
                        break;
                    case 3:
                        console.log('Timeout');
                        break;
                }
            }
        );
    };
  
  }

  ngOnInit() {
      
  }
  
  findWeather(txt) : string {
      if(txt.indexOf("Partly Cloudy")>-1){
        if(this.hours>4 && this.hours<20){
            return ("partly cloudy day");
        }
        else return ("partly cloudy night");
      }
      else if(txt.indexOf("Cloudy")>-1){
        //console.log("cloudy");
        return ("cloudy");
      }
      else if(txt.indexOf("Sunny")>-1){
        //console.log("sunny");
        return ("sunny");
      }
      else if(txt.indexOf("Snow")>-1){
        //console.log("snowy");
        return ("snowy");
      }
      else if(txt.indexOf("Rainy")>-1 || txt.indexOf("Showers")>-1){
        //console.log("rainy");
        return ("rainy");
      }
      else if(txt.indexOf("Thunder")>-1){
        //console.log("thunder");
        return ("thunder");
      }
  }
  
  ngOnDestroy() {
      this.weatherSub.unsubscribe();
  }
}
interface forecast{
    day : string;
    high : string;
    low : string;
    text : string;
}


import { Component, OnInit, OnDestroy } from '@angular/core';
import { NewsService } from '../../services/news.service';
import { ConfigService } from '../../services/config.service';
import { Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css'],
  providers: [NewsService]
})
export class NewsComponent implements OnInit, OnDestroy {
  news;
  configs;
  headline : string;
  source : string;
  i : number;
  startHeadline : boolean;
  startSource : boolean;
  dateToday : number;
  pubDate : number;
  hours : number;
  min : number;
  time : string;
  private newsSub : Subscription;
  constructor(private newsService : NewsService, private http : HttpClient, private configService : ConfigService) {
    this.i = 1;
    this.startHeadline = false;
    this.startSource = false;
    configService.getConfig().subscribe(configData => {
      this.configs = configData;
      console.log(this.configs.configSettings);
    this.newsSub = this.newsService.getNews(this.configs).subscribe(news => {
        var s;
        this.news = news;
        this.source = this.news.articles[0].source.name;
        this.headline = this.news.articles[0].title;
        this.pubDate = Number(this.news.articles[0].publishedAt.substr(8,2));
        this.dateToday = new Date().getDate();
        s = this.dateToday-this.pubDate;
        if(s>0){
          if(s==1)
            this.time = "1 day ago:";
          else this.time = s+" days ago:";
        }
        else{
          this.pubDate = Number(this.news.articles[0].publishedAt.substr(11,2));
          this.dateToday = new Date().getHours();
          s = this.dateToday-this.pubDate;
          console.log(this.pubDate +" "+ this.dateToday);
          if(s>0){
            if(s==1)
              this.time = "1 hour ago:";
            else this.time = s+" hours ago:";
          }
          else{
            this.pubDate = Number(this.news.articles[0].publishedAt.substr(14,2));
            this.dateToday = new Date().getMinutes();
            s = this.dateToday-this.pubDate;
            if(s>0){
              if(s==1)
                this.time = "1 minute ago:";
              else this.time = s+" minutes ago:";
            }
            else this.time = "Few seconds earlier: ";
          }
        }
        this.startHeadline = true;
        this.startSource = true;
        this.getHeadline();
    });
  });
  }

  ngOnInit() {
  }
  
  getHeadline(){
    setInterval(()=>{
      var s;
      this.i = this.i%20;
      this.startHeadline = false;
      this.startSource = false;
      this.headline = this.news.articles[this.i].title;
      this.source = this.news.articles[this.i].source.name;
      this.pubDate = Number(this.news.articles[this.i].publishedAt.substr(8,2));
        this.dateToday = new Date().getDate();
        s = this.dateToday-this.pubDate;
        if(s>0){
          if(s==1)
            this.time = "1 day ago:";
          else this.time = s+" days ago:";
        }
        else{
          this.pubDate = Number(this.news.articles[this.i].publishedAt.substr(11,2));
          this.dateToday = new Date().getHours();
          s = this.dateToday-this.pubDate;
          console.log(this.pubDate +" "+ this.dateToday);
          if(s>0){
            if(s==1)
              this.time = "1 hour ago:";
            else this.time = s+" hours ago:";
          }
          else{
            this.pubDate = Number(this.news.articles[this.i].publishedAt.substr(14,2));
            this.dateToday = new Date().getMinutes();
            s = this.dateToday-this.pubDate;
            if(s>0){
              if(s==1)
                this.time = "1 minute ago:";
              else this.time = s+" minutes ago:";
            }
            else this.time = "Few seconds earlier: ";
          }
        }
        
      this.startHeadline = true;
      this.startSource = true;
      this.i = this.i+1;
    },9900);
  }
  
  ngOnDestroy() {
    this.newsSub.unsubscribe();
  }
}

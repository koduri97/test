import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.css']
})
export class ClockComponent implements OnInit {
monthArr: string[];
      dayArr: string[];
      myDate: Date;
      month: string;
      day: string;
      date: string;
      year: number;
      hours: string;
      minutes: string;
      seconds: string;
      
      constructor() {
        this.monthArr = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        this.dayArr = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
      }
    
      ngOnInit() {
        this.getTime();
      }
      
      getTime(): void {
        setInterval(() => {         
        this.myDate = new Date();
        this.month = this.monthArr[this.myDate.getMonth()];
        this.day = this.dayArr[this.myDate.getDay()];
        this.year = this.myDate.getFullYear();
        
        this.date = (""+this.myDate.getDate());
        if(this.date.length==1)this.date = "0"+this.date;
        
        this.hours = (""+this.myDate.getHours());
        if(this.hours.length==1)this.hours = "0"+this.hours;
        
        this.minutes = (""+this.myDate.getMinutes());
        if(this.minutes.length==1)this.minutes = "0"+this.minutes;
        
        this.seconds = (""+this.myDate.getSeconds());
        if(this.seconds.length==1)this.seconds = "0"+this.seconds;
        }, 1000);
      }
}

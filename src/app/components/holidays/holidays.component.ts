import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Holidays } from './holidays.model';

@Component({
  selector: 'app-holidays',
  templateUrl: './holidays.component.html',
  styleUrls: ['./holidays.component.css']
})
export class HolidaysComponent implements OnInit {

  hObject;
  constructor(private http : HttpClient) { }

  ngOnInit() {
    this.http.get('http://localhost:3000/api/holidays')
    .subscribe((holidayData)=>{
      this.hObject = holidayData;
    });
  }

}
